FROM python:3.8-alpine

WORKDIR /app

ENV PORT 5555

COPY . .

RUN pip install -r requirements.txt

CMD ["python", "main.py"]
